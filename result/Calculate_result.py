#encoding=utf-8
'''
Created on Mar 25, 2018

@author: liyuying
'''

import MySQLdb


def categoryNum(name):
    if name == "安全":
        return 0
    if name == "安装失败":
        return 1
    if name == "不正常退出":
        return 2
    if name == "登录异常":
        return 3
    if name == "更新异常":
        return 4
    if name == "功能不完整":
        return 5
    if name == "性能":
        return 6
    if name == "页面布局缺陷":
        return 7
    if name == "用户体验":
        return 8
    if name == "注册异常":
        return 9
    if name == "其他":
        return 10

def calculate(appID):


    conn = MySQLdb.connect(host='localhost', user='root', passwd='root', port=3306)

    cur = conn.cursor()
    conn.select_db('Kikbug')

    cur.execute("SET character_set_client = utf8;")
    cur.execute("SET character_set_connection = utf8;")
    cur.execute("SET character_set_results = utf8;")
    cur.execute("SET character_set_server = utf8;")

    # 分类1结果：
    sql = "select bug_id,bug_category1, text_category1, image_category1,final_category1 from training_set where case_id = %s ORDER BY bug_id"
    cur.execute(sql,[appID])
    res = cur.fetchall()
    conn.commit()

    res_lst = []

    #   获取所有数据
    for one in res:
        one_res = []
        bug_id = str(one[0]).replace('L', '').replace('(', '').replace(')', '').replace(',', '')
        bug_cat = categoryNum(one[1])
        text_cat = categoryNum(one[2])
        final = categoryNum(one[4])
        one_res.append(bug_id)
        one_res.append(bug_cat)
        one_res.append(text_cat)
        if one[3] != None:
            img_cat = categoryNum(one[3])
        else:
            img_cat = 100
        one_res.append(img_cat)
        one_res.append(final)
        res_lst.append(one_res)

    #   计算正确率
    right_text = 0
    right_img = 0
    right_final = 0
    all = len(res_lst)
    for one in res_lst:
        if one[1] == one[2]:
            right_text += 1
        if one[1] == one[3]:
            right_img += 1
        if one[1] == one[4]:
            right_final += 1

    #   text正确率
    pre_text = float(right_text) / float(all)
    #   img正确率
    pre_image = float(right_img) / float(all)
    #   final正确率
    pre_final = float(right_final) / float(all)

    print "分类1文本正确率："
    print pre_text

    print "分类1图片正确率："
    print pre_image

    print "分类1报告正确率："
    print pre_final

    #  分类2结果：
    sql = "select bug_id,bug_category2, text_category2, image_category2,final_category2 from training_set where case_id = %s ORDER BY bug_id"
    cur.execute(sql, [appID])
    res = cur.fetchall()
    conn.commit()

    res_lst = []

    #   获取所有数据
    for one in res:
        one_res = []
        bug_id = str(one[0]).replace('L', '').replace('(', '').replace(')', '').replace(',', '')
        bug_cat = categoryNum(one[1])
        text_cat = categoryNum(one[2])
        one_res.append(bug_id)
        one_res.append(bug_cat)
        one_res.append(text_cat)
        final = categoryNum(one[4])
        if one[3] != None:
            img_cat = categoryNum(one[3])
        else:
            img_cat = 100
        one_res.append(img_cat)
        one_res.append(final)

        res_lst.append(one_res)

    #   计算正确率
    right_text = 0
    right_img = 0
    right_final = 0
    all = len(res_lst)
    for one in res_lst:
        if one[1] == one[2]:
            right_text += 1
        if one[1] == one[3]:
            right_img += 1
        if one[1] == one[4]:
            right_final += 1

    #   text正确率
    pre_text = float(right_text) / float(all)
    #   img正确率
    pre_image = float(right_img) / float(all)
    #   final正确率
    pre_final = float(right_final) / float(all)

    print "分类2文本正确率："
    print pre_text

    print "分类2图片正确率："
    print pre_image

    print "分类2报告正确率："
    print pre_final

if __name__ == "__main__":
    calculate("10010000000037")