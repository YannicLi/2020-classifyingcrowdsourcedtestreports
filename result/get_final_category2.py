#encoding=utf-8
'''
Created on Mar 29, 2018

@author: liyuying
'''

import MySQLdb

def categoryStr(num):
    if num == 0:
        return "安全"
    if num == 1:
        return "安装失败"
    if num == 2:
        return "不正常退出"
    if num == 3:
        return "登录异常"
    if num == 4:
        return "更新异常"
    if num == 5:
        return "功能不完整"
    if num == 6:
        return "性能"
    if num == 7:
        return "页面布局缺陷"
    if num == 8:
        return "用户体验"
    if num == 9:
        return "注册异常"
    if num == 10:
        return "其他"

def categoryNum(name):
    if name == "安全":
        return 0
    if name == "安装失败":
        return 1
    if name == "不正常退出":
        return 2
    if name == "登录异常":
        return 3
    if name == "更新异常":
        return 4
    if name == "功能不完整":
        return 5
    if name == "性能":
        return 6
    if name == "页面布局缺陷":
        return 7
    if name == "用户体验":
        return 8
    if name == "注册异常":
        return 9
    if name == "其他":
        return 10

def get_final_category(appID):
    conn = MySQLdb.connect(host='localhost', user='root', passwd='root', port=3306)

    cur = conn.cursor()
    conn.select_db('Kikbug')

    cur.execute("SET character_set_client = utf8;")
    cur.execute("SET character_set_connection = utf8;")
    cur.execute("SET character_set_results = utf8;")
    cur.execute("SET character_set_server = utf8;")

    # 分类2结果：
    sql = "select bug_id, text_category2, text_category2_proba,image_category2, image_category2_proba from training_set where case_id = %s ORDER BY bug_id"
    cur.execute(sql, [appID])
    res = cur.fetchall()
    conn.commit()

    for one_bug in res:
        bug_id = str(one_bug[0]).replace('L', '').replace('(', '').replace(')', '').replace(',', '')
        # print one_bug[1] #text_category
        text_category2_proba = float(one_bug[2]) #text_category2_proba
        if one_bug[3] == None:
            final = one_bug[1]
        else:
            image_category2_proba = float(one_bug[4]) #text_category2_proba
            if text_category2_proba >= image_category2_proba:
                final = one_bug[1]
            else:
                final = one_bug[3]

        sql = "update training_set set final_category2 = %s where bug_id = %s"
        cur.execute(sql, [final, bug_id])
        conn.commit()

if __name__ == "__main__":
    get_final_category("10010000000037")