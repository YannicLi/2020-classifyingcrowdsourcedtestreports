#encoding=utf-8
'''
Created on Mar 18, 2018

@author: liyuying
'''
import MySQLdb
conn=MySQLdb.connect(host='localhost',user='root',passwd='root',port=3306)
    
cur=conn.cursor()
conn.select_db('Kikbug')

cur.execute("SET character_set_client = utf8;")
cur.execute("SET character_set_connection = utf8;")
cur.execute("SET character_set_results = utf8;")    
cur.execute("SET character_set_server = utf8;")
sql = "update distance set is_duplicate_text=null"
cur.execute(sql)
res = cur.fetchall()
conn.commit()