#encoding=utf-8
'''
Created on Mar 18, 2018

@author: liyuying
'''

import MySQLdb
import xlrd

def insertCategory():
    conn = MySQLdb.connect(host='localhost', user='root', passwd='root', port=3306)
    cur = conn.cursor()
    conn.select_db('Kikbug')
    cur.execute("SET character_set_client = utf8;")
    cur.execute("SET character_set_connection = utf8;")
    cur.execute("SET character_set_results = utf8;")
    cur.execute("SET character_set_server = utf8;")


    category = xlrd.open_workbook("/Users/liyuying/Documents/workspace/ClassifyingCrowdsourcedTestReports/ClassifyingCrowdsourcedTestReports/Text/lib/MyListening.xls")
    sheet = category.sheet_by_name("MyListening")
    sql = "INSERT INTO refine_category(bug_id, category) VALUES (%s, %s)"
    for r in range(1, sheet.nrows):
        bug_id = int(sheet.cell(r,0).value)
        category = (sheet.cell(r, 3).value).encode('utf-8')
        print type(bug_id)
        print type(category)


        values = (bug_id, category)

        # 执行sql语句
        cur.execute(sql, values)

    res = cur.fetchall()
    conn.commit()

if __name__ == "__main__":
    insertCategory()