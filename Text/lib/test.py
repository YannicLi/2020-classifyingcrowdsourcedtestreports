#encoding=utf-8
'''
Created on Aug 21, 2017

@author: liyuying
'''

import os

import collections
from _ast import Not

rootdir = '/Users/liyuying/Desktop/output_report/Slife'
fsynonym = open('/Users/liyuying/Documents/workspace/BugReport/Text/lib/synonym.txt','rb')
fr_list = fsynonym.read()
synonym = fr_list.split('\n')

list = os.listdir(rootdir) #列出文件夹下所有的目录与文件
account = 0
synonymStatistics = {}
dict = {}
synonymDict = {}
p = 0
correspondenceDict = {}

for i in range(0,len(list)):
    path = os.path.join(rootdir,list[i])
    if os.path.isfile(path):
        #print path
        oneReport = ""
        oneReportAllLine = []
        fr = open(path,'rb')
        print path
        '''
        savePath = '/Users/liyuying/Desktop/report/Slife/'+str(list[i])
        fw = open(savePath,'a+')
        '''
        oneReport = fr.read()
        oneReportAllLine = oneReport.split('\n')
        for oneLine in oneReportAllLine:
            words = oneLine.split(" ")
            for oneWord in words:
                tmpStr = ""
                word = []
                for c in oneWord:
                    if c =="_" or c.isalpha():
                        tmpStr = tmpStr+c
                    tmpOneWord = oneWord.replace(tmpStr,'')
                word.append(tmpOneWord)
                word.append(tmpStr)
                #tmpOneWord = tmpOneWord.encode('utf-8')
                
                tmpWord =""
                flag = 0
                tmpList = []
                for aGroupOfSynonyms in synonym:
                    aGroupOfSynonyms = aGroupOfSynonyms.decode("gbk").encode("utf8")
                    oneGroupOfSynonyms = aGroupOfSynonyms.split()
                    if oneGroupOfSynonyms!="":
                        for s in oneGroupOfSynonyms: 
                            if s == tmpOneWord:
                                if flag == 1:
                                    tmpWord = tmpWord+";"+oneGroupOfSynonyms[0]
                                    if oneGroupOfSynonyms[0] not in correspondenceDict.keys():
                                        correspondenceDict[oneGroupOfSynonyms[0]] = oneGroupOfSynonyms[1]
                                    account = account + 1
                                    if oneGroupOfSynonyms[0] in synonymStatistics.keys():
                                        synonymStatistics[oneGroupOfSynonyms[0]] = synonymStatistics[oneGroupOfSynonyms[0]] + 1
                                    else:
                                        synonymStatistics[oneGroupOfSynonyms[0]] = 1
                                    break
                                if flag == 0:
                                    tmpWord = oneGroupOfSynonyms[0]
                                    if oneGroupOfSynonyms[0] not in correspondenceDict.keys():
                                        correspondenceDict[oneGroupOfSynonyms[0]] = tmpOneWord
                                    #print "tmpWord="+tmpWord
                                    flag = 1
                                    if oneGroupOfSynonyms[0] in synonymStatistics.keys():
                                        synonymStatistics[oneGroupOfSynonyms[0]] = synonymStatistics[oneGroupOfSynonyms[0]] + 1
                                    else:
                                        synonymStatistics[oneGroupOfSynonyms[0]] = 1
                                    break
                if flag == 0:
                    tmpWord = tmpOneWord
                synonymDict[tmpOneWord] = tmpWord
                tmpList.append(tmpWord+" ")
                if tmpWord in dict.keys():
                    dict[tmpWord] = dict[tmpWord] + 1
                else:
                    dict[tmpWord] = 1
                    
                    
tmp = dict.copy()
result = {}
for key in synonymDict:
    num = {}
    tmpKey = synonymDict[key]
    if ";" in tmpKey:
        #mergeSynonym = 
        for oneMergeSynonym in tmpKey.split(";"):
            num[oneMergeSynonym] = synonymStatistics[oneMergeSynonym]
        res = sorted(num,key=lambda x:num[x])[-1]
        result[key] = res
    else:
        result[key] = tmpKey
        #print tmpKey

    
for i in range(0,len(list)):
    path = os.path.join(rootdir,list[i])
    if os.path.isfile(path):
        #print path
        oneReport = ""
        oneReportAllLine = []
        fr = open(path,'rb')
        print path
        
        savePath = '/Users/liyuying/Desktop/report/Slife/'+str(list[i])
        fw = open(savePath,'a+')
        
        oneReport = fr.read()
        oneReportAllLine = oneReport.split('\n')
        for oneLine in oneReportAllLine:
            if oneLine != "":
                words = oneLine.split(" ")
                for oneWord in words:
                    tmpStr = ""
                    word = []
                    for c in oneWord:
                        if c =="_" or c.isalpha():
                            tmpStr = tmpStr+c
                        tmpOneWord = oneWord.replace(tmpStr,'')
                    for key in result:
                        #print "d="+d
                        #print "tmpOneWord="+tmpOneWord
                        if tmpOneWord == key:
                            if result[key] in correspondenceDict.keys():
                                fw.write(correspondenceDict[result[key]]+tmpStr+' ')
                            else:
                                fw.write(result[key]+tmpStr+' ')
                fw.write("\n")
fr.close()
fw.close()