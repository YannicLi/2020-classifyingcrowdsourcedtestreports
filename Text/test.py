
import os
SHOGUN_DATA_DIR=os.getenv('SHOGUN_DATA_DIR', '../../../data')
import numpy as np

from shogun import GaussianNaiveBayes
from shogun import RealFeatures
from shogun import MulticlassLabels
import classifier1
import shogun

train_data_tmp, test_data_tmp, train_target_tmp = classifier1.NB("10010000000037", 0)

features_train = RealFeatures(train_data_tmp)
class_labels = MulticlassLabels(train_target_tmp)

machine = GaussianNaiveBayes()
# print train_target_tmp
# print test_data_tmp
machine.set_features(features_train)
machine.set_labels(class_labels)

machine.train()

features_train = RealFeatures(test_data_tmp)

labels_predict = machine.apply_multiclass(features_train).get_labels()

print labels_predict

