#encoding=utf-8
'''
Created on Mar 21, 2018

@author: liyuying
'''

import MySQLdb
import random
def categoryStr(num):
    if num == 0:
        return "不正常退出"
    if num == 1:
        return "功能不完整"
    if num == 2:
        return "性能"
    if num == 3:
        return "页面布局缺陷"
    if num == 4:
        return "用户体验"
    if num == 5:
        return "其他"


def splitDataSet(appID):
    conn = MySQLdb.connect(host='localhost', user='root', passwd='root', port=3306)
    cur = conn.cursor()
    conn.select_db('Kikbug')
    cur.execute("SET character_set_client = utf8;")
    cur.execute("SET character_set_connection = utf8;")
    cur.execute("SET character_set_results = utf8;")
    cur.execute("SET character_set_server = utf8;")

    lst = [n for n in range(0, 6)]
    random.shuffle(lst)
    flag = 0

    for cat in range(0, 6):
        sql = "select bug.id from bug,report where bug.report_id = report.id AND report.case_id = %s AND bug.bug_category2 = %s order by bug.id"
        cur.execute(sql, [appID,categoryStr(cat)])
        bug_id_list = cur.fetchall()
        conn.commit()
        bug_list = []

        for one_bug in bug_id_list:
            bug_id = str(one_bug[0]).replace('L', '').replace('(', '').replace(')', '').replace(',', '')
            sql = "update training_set set group_id2=%s  where bug_id = %s"
            cur.execute(sql, [str(lst[flag]), bug_id])
            conn.commit()
            flag += 1
            if flag > 5:
                flag = 0

if __name__ == "__main__":
    splitDataSet("10010000000037")