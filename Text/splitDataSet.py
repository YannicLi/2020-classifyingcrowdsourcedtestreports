#encoding=utf-8
'''
Created on Mar 21, 2018

@author: liyuying
'''

import MySQLdb
import random
def categoryStr(num):
    if num == 0:
        return "安全"
    if num == 1:
        return "安装失败"
    if num == 2:
        return "不正常退出"
    if num == 3:
        return "登录异常"
    if num == 4:
        return "更新异常"
    if num == 5:
        return "功能不完整"
    if num == 6:
        return "性能"
    if num == 7:
        return "页面布局缺陷"
    if num == 8:
        return "用户体验"
    if num == 9:
        return "注册异常"
    if num == 10:
        return "其他"

def categoryNum(name):
    if name == "安全":
        return 0
    if name == "安装失败":
        return 1
    if name == "不正常退出":
        return 2
    if name == "登录异常":
        return 3
    if name == "更新异常":
        return 4
    if name == "功能不完整":
        return 5
    if name == "性能":
        return 6
    if name == "页面布局缺陷":
        return 7
    if name == "用户体验":
        return 8
    if name == "注册异常":
        return 9
    if name == "其他":
        return 10

def splitDataSet(appID):
    conn = MySQLdb.connect(host='localhost', user='root', passwd='root', port=3306)
    cur = conn.cursor()
    conn.select_db('kikbug')
    cur.execute("SET character_set_client = UTF8MB4;")
    cur.execute("SET character_set_connection = UTF8MB4;")
    cur.execute("SET character_set_results = UTF8MB4;")
    cur.execute("SET character_set_server = UTF8MB4;")

    lst = [n for n in range(0, 11)]
    random.shuffle(lst)
    flag = 0

    for cat in range(0, 11):
        # sql = "select bug.id,bug.bug_category1,bug.bug_category2,bug.description,bug.img_url from bug,report where bug.report_id = report.id AND report.case_id = %s AND bug.bug_category1 = %s order by bug.id"
        # cur.execute(sql, [appID,categoryStr(cat)])
        sql = "select bug.id,bug.bug_category1,bug.bug_category2,bug.description,bug.img_url from bug,report where bug.report_id = report.id AND report.case_id = %s order by bug.id"
        cur.execute(sql, [appID])

        bug_id_list = cur.fetchall()
        conn.commit()
        bug_list = []
        print bug_id_list

        for one_bug in bug_id_list:
            print "one_bug"
            bug_id = str(one_bug[0]).replace('L', '').replace('(', '').replace(')', '').replace(',', '')
            sql = "insert into training_set (bug_id,bug_category1,bug_category2,description,case_id,img_url,group_id1) values(%s,%s,%s,%s,%s,%s,%s)"
            cur.execute(sql, [bug_id, one_bug[1], one_bug[2], one_bug[3], appID, one_bug[4], str(lst[flag])])
            conn.commit()
            print "OK"
            flag += 1
            if flag > 10:
                flag = 0

if __name__ == "__main__":
    splitDataSet("10010000000032")