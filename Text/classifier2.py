
#encoding=utf-8
'''
Created on Mar 21, 2018

@author: liyuying
'''

from sklearn.naive_bayes import GaussianNB
import MySQLdb
import numpy as np
from sklearn import neighbors
from sklearn import svm
from sklearn.neural_network import MLPClassifier

def categoryStr(num):
    if num == 0:
        return "不正常退出"
    if num == 1:
        return "功能不完整"
    if num == 2:
        return "性能"
    if num == 3:
        return "页面布局缺陷"
    if num == 4:
        return "用户体验"
    if num == 5:
        return "其他"

def categoryNum(name):
    if name == "不正常退出":
        return 0
    if name == "功能不完整":
        return 1
    if name == "性能":
        return 2
    if name == "页面布局缺陷":
        return 3
    if name == "用户体验":
        return 4
    if name == "其他":
        return 5

def NB(appID):

    conn = MySQLdb.connect(host='localhost', user='root', passwd='root', port=3306)

    cur = conn.cursor()
    conn.select_db('Kikbug')

    cur.execute("SET character_set_client = utf8;")
    cur.execute("SET character_set_connection = utf8;")
    cur.execute("SET character_set_results = utf8;")
    cur.execute("SET character_set_server = utf8;")

    #训练集
    sql = "select bug_id,bug_category2 from training_set where case_id = %s ORDER BY bug_id"
    cur.execute(sql,[appID])
    res = cur.fetchall()
    conn.commit()
    train_target = []
    train_data = []
    for one in res:
        one_bug_vector = str(one[0]).replace('L', '').replace('(', '').replace(')', '').replace(',', '')
        # print "bug_id:"
        # print one_bug_vector
        train_target.append(categoryNum(one[1]))
        sql = "select text_vector from vector where bug_id = %s"
        cur.execute(sql,[one_bug_vector])
        res_vector = cur.fetchone()
        conn.commit()
        one_train_data = str(res_vector).replace('[','').replace(']','').replace('\'','').replace('(','').replace(')','').replace('\n','').split(',')
        del one_train_data[100]
        one_train_data = [float(x) for x in one_train_data]
        # print "data:"
        # print one_train_data
        train_data.append(one_train_data)

    return train_data, train_target

def insertResult(result, result_prob,appID,group_id2):
    conn = MySQLdb.connect(host='localhost', user='root', passwd='root', port=3306)

    cur = conn.cursor()
    conn.select_db('Kikbug')

    cur.execute("SET character_set_client = utf8;")
    cur.execute("SET character_set_connection = utf8;")
    cur.execute("SET character_set_results = utf8;")
    cur.execute("SET character_set_server = utf8;")

    sql = "select bug_id from training_set where case_id = %s and group_id2 = %s ORDER BY bug_id"
    cur.execute(sql, [appID,group_id2])
    res = cur.fetchall()
    conn.commit()
    flag = 0
    for one in res:
        sql = "update training_set set text_category2 = %s,text_category2_proba=%s where bug_id = %s and group_id2 = %s"
        bug_id = str(one).replace('L', '').replace('(', '').replace(')', '').replace(',', '')

        test_category = categoryStr(result[flag])
        cur.execute(sql,[test_category,result_prob[flag],bug_id,group_id2])
        conn.commit()
        flag = flag + 1


if __name__ == "__main__":
    # for i in range(0,6):
    #     result, result_prob = NB("10010000000037",i)
    #     insertResult(result, result_prob, "10010000000037",i)

    result, result_prob = NB("10010000000037")
