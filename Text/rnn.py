#encoding=utf-8
'''
Created on Mar 31, 2018

@author: liyuying
'''

#encoding=utf-8
'''
Created on Mar 21, 2018

@author: liyuying
'''


import numpy as np
from keras.datasets import imdb
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence
import MySQLdb
from keras.layers import SimpleRNN, Activation, Dense
from keras.optimizers import Adam

def categoryStr(num):
    if num == 0:
        return "安全"
    if num == 1:
        return "安装失败"
    if num == 2:
        return "不正常退出"
    if num == 3:
        return "登录异常"
    if num == 4:
        return "更新异常"
    if num == 5:
        return "功能不完整"
    if num == 6:
        return "性能"
    if num == 7:
        return "页面布局缺陷"
    if num == 8:
        return "用户体验"
    if num == 9:
        return "注册异常"
    if num == 10:
        return "其他"

def categoryNum(name):
    if name == "安全":
        return 0
    if name == "安装失败":
        return 1
    if name == "不正常退出":
        return 2
    if name == "登录异常":
        return 3
    if name == "更新异常":
        return 4
    if name == "功能不完整":
        return 5
    if name == "性能":
        return 6
    if name == "页面布局缺陷":
        return 7
    if name == "用户体验":
        return 8
    if name == "注册异常":
        return 9
    if name == "其他":
        return 10

def RNN(appID,group_id1):

    conn = MySQLdb.connect(host='localhost', user='root', passwd='root', port=3306)

    cur = conn.cursor()
    conn.select_db('Kikbug')

    cur.execute("SET character_set_client = utf8;")
    cur.execute("SET character_set_connection = utf8;")
    cur.execute("SET character_set_results = utf8;")
    cur.execute("SET character_set_server = utf8;")

    #训练集
    sql = "select bug_id,bug_category1 from training_set where case_id = %s and group_id1 != %s ORDER BY bug_id"
    cur.execute(sql,[appID,group_id1])
    res = cur.fetchall()
    conn.commit()
    train_target = []
    train_data = []
    for one in res:
        one_bug_vector = str(one[0]).replace('L', '').replace('(', '').replace(')', '').replace(',', '')
        # print "bug_id:"
        # print one_bug_vector
        train_target.append(categoryNum(one[1]))
        sql = "select text_vector from vector where bug_id = %s"
        cur.execute(sql,[one_bug_vector])
        res_vector = cur.fetchone()
        conn.commit()
        one_train_data = str(res_vector).replace('[','').replace(']','').replace('\'','').replace('(','').replace(')','').replace('\n','').split(',')
        del one_train_data[100]
        one_train_data = [float(x) for x in one_train_data]
        # print "data:"
        # print one_train_data
        train_data.append(one_train_data)



    #测试集
    sql = "select bug_id,bug_category1 from training_set where case_id = %s and group_id1 = %s ORDER BY bug_id"
    cur.execute(sql, [appID,group_id1])
    res = cur.fetchall()
    conn.commit()
    test_data = []

    for one in res:
        one_bug_vector = str(one[0]).replace('L', '').replace('(', '').replace(')', '').replace(',', '')
        # print "testing bug_id:"
        # print one_bug_vector
        sql = "select text_vector from vector where bug_id = %s"
        cur.execute(sql, [one_bug_vector])
        res_vector = cur.fetchone()
        conn.commit()
        one_test_data = str(res_vector).replace('[', '').replace(']', '').replace('\'', '').replace('(', '').replace(')','').replace('\n', '').split(',')
        del one_test_data[100]
        one_test_data = [float(x) for x in one_test_data]
        # print "testing data:"
        # print one_test_data
        test_data.append(one_test_data)

    result = []
    result_prob = []

    # train_data, train_target
    # test_data

    TIME_STEPS = 28  # same as the height of the image
    INPUT_SIZE = 28  # same as the width of the image
    BATCH_SIZE = 50
    BATCH_INDEX = 0
    OUTPUT_SIZE = 10
    CELL_SIZE = 50
    LR = 0.001

    train_set = np.array(train_data)
    target_set = np.array(train_target)
    test_set = np.array(test_data)

    # build RNN model
    model = Sequential()

    # RNN cell
    model.add(SimpleRNN(
        # for batch_input_shape, if using tensorflow as the backend, we have to put None for the batch_size.
        # Otherwise, model.evaluate() will get error.
        batch_input_shape=(None, TIME_STEPS, INPUT_SIZE),  # Or: input_dim=INPUT_SIZE, input_length=TIME_STEPS,
        output_dim=CELL_SIZE,
        unroll=True,
    ))

    # output layer
    model.add(Dense(OUTPUT_SIZE))
    model.add(Activation('softmax'))

    # optimizer
    adam = Adam(LR)
    model.compile(optimizer=adam,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])

    # training
    for step in range(4001):
        # data shape = (batch_num, steps, inputs/outputs)
        X_batch = train_set[BATCH_INDEX: BATCH_INDEX + BATCH_SIZE, :, :]
        # Y_batch = y_train[BATCH_INDEX: BATCH_INDEX + BATCH_SIZE, :]
        # cost = model.train_on_batch(X_batch, Y_batch)
        BATCH_INDEX += BATCH_SIZE
        BATCH_INDEX = 0 if BATCH_INDEX >= train_set.shape[0] else BATCH_INDEX

        # if step % 500 == 0:
        #     cost, accuracy = model.evaluate(X_test, y_test, batch_size=y_test.shape[0], verbose=False)
        #     print('test cost: ', cost, 'test accuracy: ', accuracy)

# def insertResult(result, result_prob,appID,group_id1):
#     conn = MySQLdb.connect(host='localhost', user='root', passwd='root', port=3306)
#
#     cur = conn.cursor()
#     conn.select_db('Kikbug')
#
#     cur.execute("SET character_set_client = utf8;")
#     cur.execute("SET character_set_connection = utf8;")
#     cur.execute("SET character_set_results = utf8;")
#     cur.execute("SET character_set_server = utf8;")
#
#     sql = "select bug_id from training_set where case_id = %s and group_id1 = %s ORDER BY bug_id"
#     cur.execute(sql, [appID,group_id1])
#     res = cur.fetchall()
#     conn.commit()
#     flag = 0
#     for one in res:
#         sql = "update training_set set text_category1 = %s,text_category1_proba=%s where bug_id = %s and group_id1 = %s"
#         bug_id = str(one).replace('L', '').replace('(', '').replace(')', '').replace(',', '')
#         test_category = categoryStr(result[flag])
#         cur.execute(sql,[test_category,result_prob[flag],bug_id,group_id1])
#         conn.commit()
#         flag = flag + 1


if __name__ == "__main__":
    appID = "10010000000037"
    RNN(appID,0)

    # for i in range(0,11):
        # result, result_prob = NB("10010000000037",i)
        # insertResult(result, result_prob, "10010000000037",i)



