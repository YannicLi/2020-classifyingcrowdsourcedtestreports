#encoding=utf-8
'''
Created on Mar 21, 2018

@author: liyuying
'''

import scipy.io as sio
import os
import MySQLdb
from sklearn.naive_bayes import GaussianNB
from sklearn import neighbors
from sklearn import svm
from sklearn.neural_network import MLPClassifier

def categoryStr(num):
    if num == 0:
        return "不正常退出"
    if num == 1:
        return "功能不完整"
    if num == 2:
        return "性能"
    if num == 3:
        return "页面布局缺陷"
    if num == 4:
        return "用户体验"
    if num == 5:
        return "其他"

def categoryNum(name):
    if name == "不正常退出":
        return 0
    if name == "功能不完整":
        return 1
    if name == "性能":
        return 2
    if name == "页面布局缺陷":
        return 3
    if name == "用户体验":
        return 4
    if name == "其他":
        return 5

def getVector(load_fn,appName,appID):

    load_data = sio.loadmat(load_fn)
    lst = ['0.0' for n in range(4200)]

    vector = load_data.values()[0]
    conn = MySQLdb.connect(host='localhost', user='root', passwd='root', port=3306)
    cur = conn.cursor()
    conn.select_db('Kikbug')
    cur.execute("SET character_set_client = utf8;")
    cur.execute("SET character_set_connection = utf8;")
    cur.execute("SET character_set_results = utf8;")
    cur.execute("SET character_set_server = utf8;")

    path = './mfiles/image/'+appName
    train_data = []
    test_data = []
    sql = "select bug_id from training_set where case_id = %s"
    cur.execute(sql,[appID])
    res = cur.fetchall()
    conn.commit()
    training_id = []
    for i in res:
        one_id = str(i).replace('L', '').replace('(', '').replace(')', '').replace(',', '')
        training_id.append(one_id)

    train = {}
    test = {}
    for root in os.walk(path):
        file_list = str(root).replace('[','').replace(']','').replace('\'','').split(',')
        file = sorted(file_list)
        del file[0]
        del file[len(file)-1]
        flag = 0
        f = 0
        for one in file:  # 当前目录路径
            if one.split('-')[0].replace(" ", '') == ".DS_Store":
                continue
            f += 1
            if flag == 0:
                bug_id_p = one.split('-')[0].replace(" ",'')
                tmp_vector = vector[flag]
                flag = flag + 1
                continue
            bug_id = one.split('-')[0].replace(" ",'')
            if bug_id == bug_id_p:
                tmp_vector = vector[flag] + tmp_vector
                flag = flag + 1
                continue
                tmp_vector.tolist()
                train[bug_id_p] = [float(x) for x in tmp_vector.tolist()]
            tmp_vector = vector[flag]
            bug_id_p = bug_id
            flag = flag + 1


    sql = "select bug_id,bug_category2 from training_set where case_id = %s ORDER BY bug_id"
    cur.execute(sql, [appID])
    res = cur.fetchall()
    conn.commit()
    train_target = []
    for one in res:
        #print ' ' + str(one[0]).replace('L', '').replace('(', '').replace(')', '').replace(',', '')
        #print train.keys()
        if str(one[0]).replace('L', '').replace('(', '').replace(')', '').replace(',', '') in train.keys():
            train_data.append(train[str(one[0]).replace('L', '').replace('(', '').replace(')', '').replace(',', '')])
        else:
            train_data.append([float(x) for x in lst])
        train_target.append(categoryNum(one[1]))

    return train_data, train_target


def insertResult(result,result_prob,appID,group_id2):
    conn = MySQLdb.connect(host='localhost', user='root', passwd='root', port=3306)

    cur = conn.cursor()
    conn.select_db('Kikbug')

    cur.execute("SET character_set_client = utf8;")
    cur.execute("SET character_set_connection = utf8;")
    cur.execute("SET character_set_results = utf8;")
    cur.execute("SET character_set_server = utf8;")

    sql = "select bug_id, img_url from training_set where case_id = %s and group_id2 = %s ORDER BY bug_id"
    cur.execute(sql, [appID,group_id2])
    res = cur.fetchall()
    conn.commit()
    test_null = []
    for bug in res:
        if str(bug[1]) == "":
            test_null.append(str(bug[0]).replace('L', '').replace('(', '').replace(')', '').replace(',', ''))
    # for a in test_null:
    #    print a


    sql = "select bug_id from training_set where case_id = %s and group_id2 = %s ORDER BY bug_id"
    cur.execute(sql, [appID,group_id2])
    res = cur.fetchall()
    conn.commit()
    flag = 0
    for one in res:
        sql = "update training_set set image_category2 = %s,image_category2_proba=%s where group_id2 = %s and bug_id = %s"
        bug_id = str(one).replace('L', '').replace('(', '').replace(')', '').replace(',', '')
        if bug_id in test_null:
            continue
        test_category = categoryStr(result[flag])
        # print "bug_id"
        # print bug_id
        cur.execute(sql,[test_category,result_prob[flag],group_id2,bug_id])
        conn.commit()
        flag = flag + 1

if __name__ == "__main__":
    appName = "HuJiang"
    load_fn = './mfiles/result/HuJiang.mat'
    appID = "10010000000037"
    # for i in range(0,6):
    #     result,result_prob = getVector(load_fn,appName,appID,i)
    #     insertResult(result,result_prob,appID,i)
    result, result_prob = getVector(load_fn, appName, appID)
